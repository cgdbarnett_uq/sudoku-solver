// board.h
//
// Defines the sudoku game board.
#ifndef BOARD_H // Header guard
#define BOARD_H

// C Libraries
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// CONSTANTS =================================================================
// Floating point number comparison precision
#define EPSILON 0.0000001

// Default buffer size for reading lines.
#define BUFFER_SIZE 80

// Sudoku rules
#define SUDOKU_MIN_VALUE 1

// Box drawing characters
#define BOX_CORNER_TL 0x6c
#define BOX_CORNER_TR 0x6b
#define BOX_CORNER_BL 0x6d
#define BOX_CORNER_BR 0x6a
#define BOX_HORIZONTAL 0x71
#define BOX_VERTICAL 0x78
#define BOX_INTERSECT_T 0x77
#define BOX_INTERSECT_B 0x76
#define BOX_INTERSECT_L 0x74
#define BOX_INTERSECT_R 0x75
#define BOX_INTERSECT_M 0x6e

// DATA STRUCTURES ===========================================================
// Stores the value of an individual cell, and whether the value is known
// or variable.
typedef struct Cell {
    int value;
    bool known;
} Cell;

// Stores the x and y position of an individual cell
typedef struct CellPosition {
    int col;
    int row;
} CellPosition;

// Stores the state of a game of Sudoku
typedef struct Sudoku {
    Cell** cells;
    int size;
    int boxSize;
    int displayWidth;
    int displayPadding;
    char* displayFormat;
    char* displayFormatEmpty;
} Sudoku;

// FUNCTION PROTOTYPES =======================================================
// Sets the display format of the sudoku board.
void set_sudoku_display_format(Sudoku* sudoku);

// Allocates memory for a blank sudoku board
void malloc_sudoku(Sudoku* sudoku);

// Creates an empty sudoku board
Sudoku create_sudoku();

// Initialises a blank sudoku board
void blank_sudoku(Sudoku* sudoku, int size);

// Frees memory allocated to a sudoku board
void free_sudoku(Sudoku* sudoku);

// Set a value of the sudoku board, and flag whether this is known
// or variable.
void set_sudoku_value(Sudoku* sudoku, CellPosition pos, int value,
        bool known);

// Returns whether the value in a given cell is known or variable.
bool is_cell_known(Sudoku* sudoku, CellPosition pos);

// Reads and validates the size of a sudoku board.
int read_size(Sudoku* sudoku);

// Reads a sudoku board from stdin.
void read_sudoku(Sudoku* sudoku);

// Prints the top outline of the board.
void print_sudoku_top(Sudoku* sudoku);

// Prints a single row of the board.
void print_sudoku_row(Sudoku* sudoku, int row);

// Prints a horizontal separator.
void print_sudoku_separator(Sudoku* sudoku);

// Prints the bottom row outline of the board.
void print_sudoku_bottom(Sudoku* sudoku);

// Prints the board to stdout.
void print_sudoku(Sudoku* sudoku);

#endif // Close header guard
