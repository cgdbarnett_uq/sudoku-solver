// board.c
//
// Defines the sudoku game board and methods for displaying it.
#include "board.h"

// MEMORY MANAGEMENT =========================================================
// Sets the display format of the sudoku board.
void set_sudoku_display_format(Sudoku* sudoku) {
    // Allocate format specifiers
    sudoku->displayWidth = sudoku->size > 9 ? 2 : 1;
    sudoku->displayPadding = sudoku->boxSize + 1;
    
    sudoku->displayFormat = malloc(sizeof(char) * 5);
    sprintf(sudoku->displayFormat, "%%%dd ", sudoku->displayWidth);
    
    sudoku->displayFormatEmpty = malloc(sizeof(char) * 
            sudoku->displayWidth + 1);
    for (int i = 0; i < sudoku->displayWidth + 2; i++) {
        if (i == sudoku->displayWidth + 1) {
            sudoku->displayFormatEmpty[i] = 0;
        } else {
            sudoku->displayFormatEmpty[i] = ' ';
        }
    }
}

// Allocates memory for a sudoku board
void malloc_sudoku(Sudoku* sudoku) {
    if (sudoku->size > 0) {
        // Allocate cell structure
        sudoku->cells = malloc(sizeof(Cell*) * sudoku->size);
        for (int i = 0; i < sudoku->size; i++) {
            sudoku->cells[i] = malloc(sizeof(Cell) * sudoku->size);
            for (int j = 0; j < sudoku->size; j++) {
                sudoku->cells[i][j].value = 0;
                sudoku->cells[i][j].known = false;
            }
        }
        
        // Set display formatters
        set_sudoku_display_format(sudoku);
    } else {
        // TODO: Handle error
    }
}

// Creates an empty sudoku board
Sudoku create_sudoku() {
    Sudoku sudoku;
    sudoku.size = 0;
    sudoku.boxSize = 0;
    return sudoku;
}

// Initialises a blank sudoku board
void blank_sudoku(Sudoku* sudoku, int size) {
    double boxSize = sqrt(size);
    if (boxSize - floor(boxSize) >= EPSILON) {
        // Invalid dimension provided
        // TODO: Handle errors in some manner
        return;
    }

    // Unallocate any currently allocated memory
    if (sudoku->size > 0) {
        free_sudoku(sudoku);
    }

    // Allocate new board
    sudoku->size = size;
    sudoku->boxSize = boxSize;
    malloc_sudoku(sudoku);
}

// Frees memory allocated to a sudoku board
void free_sudoku(Sudoku* sudoku) {
    if (sudoku->size > 0) {
        // Free cells
        for (int i = 0; i < sudoku->size; i++) {
            free(sudoku->cells[i]);
        }
        free(sudoku->cells);
        
        // Free formatters
        free(sudoku->displayFormat);
        free(sudoku->displayFormatEmpty);

        // Set size to zero so we can realloc later
        sudoku->size = 0;
        sudoku->boxSize = 0;
    }
}

// ACCESS ====================================================================
// Set a value of the sudoku board, and flag whether this is known
// or variable.
void set_sudoku_value(Sudoku* sudoku, CellPosition pos, int value,
        bool known) {
    sudoku->cells[pos.col][pos.row].value = value;
    sudoku->cells[pos.col][pos.row].known = known;
}

// Returns whether the value in a given cell is known or variable.
bool is_cell_known(Sudoku* sudoku, CellPosition pos) {
    return sudoku->cells[pos.col][pos.row].known;
}

// READING ===================================================================
// Reads and validates the size of the sudoku board.
int read_size(Sudoku* sudoku) {
    char* buffer = NULL;
    size_t bufferSize = 0;
    char dummy;
    int size;
    double boxSize;

    // Read from stdin
    if (getline(&buffer, &bufferSize, stdin) <= 0) {
        // TODO: Error handling
        if (buffer) {
            free(buffer);
        }
        return 0;
    }
    if (sscanf(buffer, "%d%1[^\n]", &size, &dummy) != 1) {
        // TODO: Error handling
        fprintf(stderr, "Size invalid.\n");
        free(buffer);
        return 0;
    }
    free(buffer);
    
    if (size <= 1) {
        // TODO: Error handling
        return 0;
    }

    boxSize = sqrt(size);
    if (boxSize - floor(boxSize) >= EPSILON) {
        // TODO: Error handling
        return 0;
    }

    return size;
}

// Reads a row of the sudoku board from stdin.
void read_row(Sudoku* sudoku, int row) {
    char* buffer = NULL;
    size_t bufferSize = 0;

    // Read line
    if (getline(&buffer, &bufferSize, stdin) <= 0) {
        // TODO: Error handling
        if (buffer) {
            free(buffer);
        }
        return;
    }

    // Iterate through line
    char remainder[bufferSize];
    memset(remainder, 0, sizeof(remainder));
    strcpy(remainder, buffer);
    for (int i = 0; i < sudoku->size; i++) {
        // Read and validate value
        CellPosition pos = {i, row};
        int value = 0;
        if (sscanf(remainder, "%d %[0-9 ]", &value, remainder) <= 0) {
            // TODO: Error handling
            fprintf(stderr, "Digits not read.\n");
            free(buffer);
            return;
        }
        if (value < 0 || value > sudoku->size) {
            // TODO: Error handling
            fprintf(stderr, "Digit invalid.\n");
            free(buffer);
            return;
        }
        set_sudoku_value(sudoku, pos, value, value != 0);
    }

    // Free memory and return
    free(buffer);
}

// Reads a sudoku board from stdin.
void read_sudoku(Sudoku* sudoku) {
    int size = read_size(sudoku);
    blank_sudoku(sudoku, size);
    for (int i = 0; i < sudoku->size; i++) {
        read_row(sudoku, i);
    }
}

// DISPLAY ===================================================================
// Prints the top outline of the board.
void print_sudoku_top(Sudoku* sudoku) {
    printf("\x1b(0%c", BOX_CORNER_TL);
    for (int i = 0; i < sudoku->boxSize; i++) {
        for (int j = 0; j < sudoku->boxSize * sudoku->displayWidth + 
                sudoku->displayPadding; j++) {
            printf("%c", BOX_HORIZONTAL);
        }
        if (i < sudoku->boxSize - 1) {
            printf("%c", BOX_INTERSECT_T);
        }
    }
    printf("%c\x1b(B\n", BOX_CORNER_TR);
}

// Prints a single row of the board.
void print_sudoku_row(Sudoku* sudoku, int row) {
    printf("\x1b(0%c\x1b(B", BOX_VERTICAL);
    for (int xBox = 0; xBox < sudoku->boxSize; xBox++) {
        printf(" ");
        for (int x = 0; x < sudoku->boxSize; x++) {
            int value = sudoku->cells[sudoku->boxSize * xBox + x][row].value;
            if (value == 0) {
                printf("%s", sudoku->displayFormatEmpty);
            } else {
                printf(sudoku->displayFormat, value);
            }
        }
        printf("\x1b(0%c\x1b(B", BOX_VERTICAL);
    }
    printf("\n");
}

// Prints a horizontal separator.
void print_sudoku_separator(Sudoku* sudoku) {
    printf("\x1b(0%c", BOX_INTERSECT_L);
    for (int xBox = 0; xBox < sudoku->boxSize; xBox++) {
        for (int x = 0; x < sudoku->boxSize * sudoku->displayWidth +
                sudoku->displayPadding; x++) {
            printf("%c", BOX_HORIZONTAL);
        }
        if (xBox < sudoku->boxSize - 1) {
            printf("%c", BOX_INTERSECT_M);
        }
    }
    printf("%c\x1b(B\n", BOX_INTERSECT_R);
}

// Prints the bottom row outline of the board.
void print_sudoku_bottom(Sudoku* sudoku) {
    printf("\x1b(0%c", BOX_CORNER_BL);
    for (int xBox = 0; xBox < sudoku->boxSize; xBox++) {
        for (int x = 0; x < sudoku->boxSize * sudoku->displayWidth +
                sudoku->displayPadding; x++) {
            printf("%c", BOX_HORIZONTAL);
        }
        if (xBox < sudoku->boxSize - 1) {
            printf("%c", BOX_INTERSECT_B);
        }
    }
    printf("%c\x1b(B\n", BOX_CORNER_BR);
}

// Prints the board to stdout.
void print_sudoku(Sudoku* sudoku) {
    print_sudoku_top(sudoku);

    // Print board values
    for (int yBox = 0; yBox < sudoku->boxSize; yBox++) {
        for (int y = 0; y < sudoku->boxSize; y++) {
            print_sudoku_row(sudoku, sudoku->boxSize * yBox + y);
        }
        if (yBox < sudoku->boxSize - 1) {
            print_sudoku_separator(sudoku);
        }
    }

    print_sudoku_bottom(sudoku);
}
