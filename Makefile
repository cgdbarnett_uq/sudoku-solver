# Compiler and flags
CC = gcc
CFLAGS = -Wall -pedantic -std=gnu99
LINKS = -lm

# Dependencies and objs
DEPS = board.h
OBJS = sudoku.o board.o

# Build c files
%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $<

# Main target
sudoku: $(OBJS)
	$(CC) $(CFLAGS) -o sudoku $(OBJS) $(LINKS)

# Clean rule
clean:
	rm -f *.o
	rm -f sudoku
