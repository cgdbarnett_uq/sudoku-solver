// sudoku.c
//
// A sudoku solver that uses a recursive algorithm to solve any valid sudoku

// C Libraries
#include <fcntl.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

// Project libraries
#include "board.h"

// SOLVER FUNCTIONS ==========================================================
// Returns whether the value in a given position is valid with respect
// to the row.
bool is_value_row_valid(Sudoku* sudoku, CellPosition pos, int value) {
    for (int i = 0; i < sudoku->size; i++) {
        if (sudoku->cells[pos.col][i].value == value) {
            return false;
        }
    }
    return true;
}

// Returns whether the value in a given position is valid with respect to
// the column.
bool is_value_col_valid(Sudoku* sudoku, CellPosition pos, int value) {
    for (int i = 0; i < sudoku->size; i++) {
        if (sudoku->cells[i][pos.row].value == value) {
            return false;
        }
    }
    return true;
}

// Returns whether the value in a given position is valid with respect to
// the box.
bool is_value_box_valid(Sudoku* sudoku, CellPosition pos, int value) {
    // Determine box bounds
    int x = (pos.col / sudoku->boxSize) * sudoku->boxSize;
    int y = (pos.row / sudoku->boxSize) * sudoku->boxSize;

    // Test for any collisions
    for (int i = 0; i < sudoku->boxSize; i++) {
        for (int j = 0; j < sudoku->boxSize; j++) {
            if (sudoku->cells[x + i][y + j].value == value) {
                return false;
            }
        }
    }
    return true;
}

// Returns whether the value in a given position is valid.
bool is_value_valid(Sudoku* sudoku, CellPosition pos, int value) {
    return is_value_row_valid(sudoku, pos, value) &&
            is_value_col_valid(sudoku, pos, value) &&
            is_value_box_valid(sudoku, pos, value);
}

// Returns next cell position to test given the current test position. If
// the end of the Sudoku is reached then the position returned will not be
// valid.
CellPosition get_next_test_position(Sudoku* sudoku, CellPosition pos) {
    CellPosition nextPos = {pos.col, pos.row};
    while ((nextPos.row == pos.row && nextPos.col == pos.col) 
            || is_cell_known(sudoku, nextPos)) {
        if (++nextPos.col >= sudoku->size) {
            nextPos.row++;
            nextPos.col = 0;
        }

        if (nextPos.row >= sudoku->size) {
            break;
        }
    }
    return nextPos;
}

// Recursive helper function for solving a sudoku.
bool solve_sudoku_helper(Sudoku* sudoku, CellPosition pos) {
    // Terminate recursion if we are out of bounds. We assume we've
    // reached this because of a successful test.
    if (pos.row >= sudoku->size || pos.col >= sudoku->size) {
        return true;
    }

    // Initialise test variable and next test position
    int value = SUDOKU_MIN_VALUE;
    CellPosition nextPos = get_next_test_position(sudoku, pos);
    while (1) {
        if (is_value_valid(sudoku, pos, value)) {
            set_sudoku_value(sudoku, pos, value, false);
            if (solve_sudoku_helper(sudoku, nextPos)) {
                break;
            }
        }
        if (++value > sudoku->size) {
            set_sudoku_value(sudoku, pos, 0, false);
            return false;
        }
    }

    return true;
}

// Attempts to solve a given sudoku.
void solve_sudoku(Sudoku* sudoku) {
    CellPosition initial = {0, 0};
    if (is_cell_known(sudoku, initial)) {
        initial = get_next_test_position(sudoku, initial);
    }

    if (!solve_sudoku_helper(sudoku, initial)) {
        printf("Unable to find solution.\n");
    } else {
        printf("Solution found.\n");
    }
}

// PROGRAM FUNCTIONS =========================================================
// Redirects stdin from the provided file.
void set_stdin_from_file(char* filename) {
    // Open file
    int fd = open(filename, O_RDONLY);
    if (fd == -1) {
        perror("Reading file");
        // TODO: Error handling.
        return;
    }

    // Redirect stdin
    dup2(fd, STDIN_FILENO);
}

// Processes command line arguments provided to process.
void process_arguments(int argc, char** argv) {
    if (argc == 1) {
        return;
    } else if (argc == 2) {
        set_stdin_from_file(argv[1]);
    } else {
        // TODO: Error handling.
        fprintf(stderr, "Invalid number of arguments passed.");
    }
}

// Main entry point
int main(int argc, char** argv) {
    // Handle arguments
    process_arguments(argc, argv);

    // Read sudok from stdin, solve and print output.
    Sudoku sudoku = create_sudoku();
    read_sudoku(&sudoku);
    print_sudoku(&sudoku);
    solve_sudoku(&sudoku);
    print_sudoku(&sudoku);
    free_sudoku(&sudoku);

    return 0;
}

